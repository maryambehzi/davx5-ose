/*
 * Copyright © Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package at.bitfire.davdroid.ui.setup

import android.app.ProgressDialog
import android.content.Intent
import android.net.MailTo
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.security.KeyChain
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import at.bitfire.davdroid.R
import at.bitfire.davdroid.databinding.LoginCredentialsFragmentBinding
import at.bitfire.davdroid.model.Credentials
import java.net.URI
import java.net.URISyntaxException


class DefaultLoginCredentialsFragment: Fragment() {

    private lateinit var model: DefaultLoginCredentialsModel
    private lateinit var loginModel: LoginModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

//        openAccountChooser()

        model = ViewModelProviders.of(this).get(DefaultLoginCredentialsModel::class.java)
        loginModel = ViewModelProviders.of(requireActivity()).get(LoginModel::class.java)

        val v = LoginCredentialsFragmentBinding.inflate(inflater, container, false)
        v.lifecycleOwner = viewLifecycleOwner
        v.model = model

        // initialize model on first call
        if (savedInstanceState == null)
            activity?.intent?.let { model.initialize(it) }

        v.selectCertificate.setOnClickListener {
            KeyChain.choosePrivateKeyAlias(requireActivity(), { alias ->
                Handler(Looper.getMainLooper()).post {
                    model.certificateAlias.value = alias
                }
            }, null, null, null, -1, model.certificateAlias.value)
        }

        v.login.setOnClickListener {
            if (validate())
                requireFragmentManager().beginTransaction()
                        .replace(android.R.id.content, DetectConfigurationFragment(), null)
                        .addToBackStack(null)
                        .commit()
        }

        return v.root
    }

    private fun validate(): Boolean {
        var valid = false

        fun validateUrl() {
            model.baseUrlError.value = null
            try {
                val uri = URI(model.baseUrl.value.orEmpty())
                if (uri.scheme.equals("http", true) || uri.scheme.equals("https", true)) {
                    valid = true
                    loginModel.baseURI = uri
                } else
                    model.baseUrlError.value = getString(R.string.login_url_must_be_http_or_https)
            } catch (e: Exception) {
                model.baseUrlError.value = e.localizedMessage
            }
        }

        fun validatePassword(): String? {
            model.passwordError.value = null
            val password = model.password.value
            if (password.isNullOrEmpty()) {
                valid = false
                model.passwordError.value = getString(R.string.login_password_required)
            }
            return password
        }

        when {
            model.loginWithEmailAddress.value == true -> {
                // login with email address
                model.usernameError.value = null
                val email = model.username.value.orEmpty()
                if (email.matches(Regex(".+@.+"))) {
                    // already looks like an email address
                    try {
                        loginModel.baseURI = URI(MailTo.MAILTO_SCHEME, email, null)
                        valid = true
                    } catch (e: URISyntaxException) {
                        model.usernameError.value = e.localizedMessage
                    }
                } else {
                    valid = false
                    model.usernameError.value = getString(R.string.login_email_address_error)
                }

                val password = validatePassword()

                if (valid)
                    loginModel.credentials = Credentials(email, password, null)
            }

            model.loginWithUrlAndUsername.value == true -> {
                validateUrl()

                model.usernameError.value = null
                val username = model.username.value
                if (username.isNullOrEmpty()) {
                    valid = false
                    model.usernameError.value = getString(R.string.login_user_name_required)
                }

                val password = validatePassword()

                if (valid)
                    loginModel.credentials = Credentials(username, password, null)
            }

            model.loginWithUrlAndCertificate.value == true -> {
                validateUrl()

                model.certificateAliasError.value = null
                val alias = model.certificateAlias.value
                if (alias.isNullOrBlank()) {
                    valid = false
                    model.certificateAliasError.value = ""      // error icon without text
                }

                if (valid)
                    loginModel.credentials = Credentials(null, null, alias)
            }
        }

        return valid
    }


//    private fun openAccountChooser() {
//        try {
//            AccountImporter.pickNewAccount(this)
//        } catch (e: NextcloudFilesAppNotInstalledException) {
//            UiExceptionManager.showDialogForException(getActivity(), e)
//        }
//
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//
//
//        AccountImporter.onActivityResult(requestCode, resultCode, data, this@DefaultLoginCredentialsFragment, AccountImporter.IAccountAccessGranted { account ->
//             As this library supports multiple accounts we created some helper methods if you only want to use one.
//             The following line stores the selected account as the "default" account which can be queried by using
//             the SingleAccountHelper.getCurrentSingleSignOnAccount(context) method
//            SingleAccountHelper.setCurrentAccount(getActivity(), account.name)
//
//
//             TODO ... (see code in section 3 and below)
//        })
//
//
//        val callback = object : NextcloudAPI.ApiConnectedListener {
//            override fun onConnected() {
//                 ignore this one..
//            }
//
//            override fun onError(ex: Exception) {
//                 TODO handle errors
//            }
//        }
//        try {
//             Get the "default" account
//            val ssoAccount = SingleAccountHelper.getCurrentSingleSignOnAccount(context)
//            var mNextcloudAPI = NextcloudAPI(context, ssoAccount, GsonBuilder().create(), callback)
//
//            fun downloadFile() {
//                val nextcloudRequest = NextcloudRequest.Builder()
//                        .setMethod("GET")
//                        .setUrl(ssoAccount.url)
//                        .build()
//
//                try {
//                    val inputStream = mNextcloudAPI!!.performNetworkRequest(nextcloudRequest)
//                    while (inputStream.available() > 0) {
//                        val input = inputStream.read()
//                         TODO do something useful with the data here..
//                         like writing it to a file..?
//                    }
//                    inputStream.close()
//                } catch (e: Exception) {
//
//                     TODO handle errors
//                }
//
//            }
//            object : Thread() {
//                override fun run() {
//                    downloadFile()
//                }
//            }.start()
//
//        }catch (e: NextcloudFilesAppAccountNotFoundException) {
//                 TODO handle errors
//        } catch (e: NoCurrentAccountSelectedException) {
//                 TODO handle errors
//        }
//
//    }
//
//
//    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//
//        AccountImporter.onRequestPermissionsResult(requestCode, permissions, grantResults , this)
//    }
//
//
//    step 2 in single sign on
//
//    private fun openAccountChooser() {
//        try {
//            AccountImporter.pickNewAccount(this)
//        } catch (e: NextcloudFilesAppNotInstalledException) {
//            UiExceptionManager.showDialogForException(activity, e)
//        }
//
//    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//
//        val callback = object : NextcloudAPI.ApiConnectedListener {
//            override fun onConnected() {
//
//                Log.d("aaa", "onConnected")
//
//                val count = Intent(activity, RetrofitActivity::class.java)
//                startActivity(count)
//
//                val apiCallback = object : NextcloudAPI.ApiConnectedListener {
//                    override fun onConnected() {
//                         //ignore this one..
//                    }
//
//                    override fun onError(ex: Exception) {
//                        // TODO handle error in your app
//                    }
//                }
//                val ssoAccount = SingleAccountHelper.getCurrentSingleSignOnAccount(context)
//                var mNextcloudAPI = NextcloudAPI(context, ssoAccount, GsonBuilder().create(), apiCallback)
//
////                val mApi = NextcloudRetrofitApiBuilder(mNextcloudAPI , )
//
//                try {
//
//
//                    fun downloadFile() {
//                        val nextcloudRequest = NextcloudRequest.Builder()
//                                .setMethod("GET")
//                                .setUrl(ssoAccount.url)
//                                .build()
//
//                        try {
//                            val inputStream = mNextcloudAPI!!.performNetworkRequest(nextcloudRequest)
//                            while (inputStream.available() > 0) {
//                                val input = inputStream.read()
//                                // TODO do something useful with the data here..
//                                // like writing it to a file..?
//                            }
//                            inputStream.close()
//                        } catch (e: Exception) {
//
//                            // TODO handle errors
//                        }
//
//                    }
//
////                 Start download of file in background thread (otherwise you'll get a NetworkOnMainThreadException)
//                    object : Thread() {
//                        override fun run() {
//                            downloadFile()
//                        }
//                    }.start()
//                } catch (e: NextcloudFilesAppAccountNotFoundException) {
////                 TODO handle errors
//                } catch (e: NoCurrentAccountSelectedException) {
////                 TODO handle errors
//                }
//
//                // ignore this one..
//            }
//
//
//            override fun onError(ex: Exception) {
//
//                Log.d("bbb", "onError")
//                // TODO handle errors
//            }
//        }
//        AccountImporter.onActivityResult(requestCode, resultCode, data, this@DefaultLoginCredentialsFragment, AccountImporter.IAccountAccessGranted { account ->
//            // As this library supports multiple accounts we created some helper methods if you only want to use one.
//            // The following line stores the selected account as the "default" account which can be queried by using
//            // the SingleAccountHelper.getCurrentSingleSignOnAccount(context) method
//            SingleAccountHelper.setCurrentAccount(activity, account.name)
//
//        })
//    }
}
