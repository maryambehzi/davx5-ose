/*
 * Copyright © Ricki Hirner (bitfire web engineering).
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 */

package at.bitfire.davdroid.ui.setup

import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import at.bitfire.davdroid.App
import at.bitfire.davdroid.R
import at.bitfire.davdroid.ui.UiUtils
import java.util.*

/**
 * Activity to initially connect to a server and create an account.
 * Fields for server/user data can be pre-filled with extras in the Intent.
 */
class LoginActivity: AppCompatActivity() {
    private fun isPackageInstalled(packageName: String, packageManager: PackageManager): Boolean {
        try {
            packageManager.getPackageInfo(packageName, 0)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
            return false
        }

    }

    companion object {
        /**
         * When set, "login by URL" will be activated by default, and the URL field will be set to this value.
         * When not set, "login by email" will be activated by default.
         */
        const val EXTRA_URL = "https://cloud.sabaos.com/remote.php/dav"

        /**
         * When set, and {@link #EXTRA_PASSWORD} is set too, the user name field will be set to this value.
         * When set, and {@link #EXTRA_URL} is not set, the email address field will be set to this value.
         */
         var EXTRA_USERNAME = ""

        /**
         * When set, the password field will be set to this value.
         */
         var EXTRA_PASSWORD = ""
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        if(isPackageInstalled("com.sabaos.saba" , packageManager) == true) {
            val intent = intent

            intent.component = ComponentName("com.sabaos.saba", "com.sabaos.saba.AccountActivity")
            startActivity(intent)

            when {
                intent?.action == Intent.ACTION_SEND -> {
                    val sharedText = intent.getStringExtra(Intent.EXTRA_TEXT)
                    val parts = (sharedText as String).split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

                    EXTRA_USERNAME = parts[0]
                    EXTRA_PASSWORD = parts[1]

                }
                else -> {
                    //Handle other intents, such as being started from the home screen
                }
            }
        }
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null)
            // first call, add first login fragment
            supportFragmentManager.beginTransaction()
                    .replace(android.R.id.content, DefaultLoginCredentialsFragment())
                    .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_login, menu)
        return true
    }

    fun showHelp(item: MenuItem) {
        UiUtils.launchUri(this, App.homepageUrl(this).buildUpon()
                .appendPath("tested-with")
                .build())
    }

}
